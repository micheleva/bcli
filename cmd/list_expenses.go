/*
Copyright © 2023 Michele Valsecchi <https://gitlab.com/micheleva>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

type ExpenseResponse struct {
	Count int       `json:"count"`
	Data  []Expense `json:"results"`
}

type Expense struct {
	ID           int64  `json:"id"`
	Amount       int    `json:"amount"`
	CategoryId   int    `json:"category_id"`
	CategoryText string `json:"category_text"`
	Note         string `json:"note"`
	Date         string `json:"date"`
}

type ExpensesQuery struct {
	CategoryID   int64
	CategoryName string
	startDate    string
	endDate      string
}

func (e Expense) String() string {
	p := message.NewPrinter(language.English)
	return p.Sprintf(
		"Category: %s, %s ¥%d '%s'\n",
		e.CategoryText, e.Date, e.Amount, e.Note,
	)
}

func (e Expense) VerbosePrint() string {
	p := message.NewPrinter(language.English)
	ID := strconv.FormatInt(e.ID, 10)
	return p.Sprintf(
		"ID: %s Amount: ¥%d Category ID: %d Category Text: %s Note: \"%s\" Date: %s\n",
		ID, e.Amount, e.CategoryId, e.CategoryText, e.Note, e.Date,
	)
}

var expensesCmd = &cobra.Command{
	Use:   "expenses",
	Short: "List expenses",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		client, err := GetClientWithCookiesOrLogin()
		if err != nil {
			log.Fatal(err)
		}

		queryParam := ExpensesQuery{
			startDate:    startDate,
			endDate:      endDate,
			CategoryID:   categoryId,
			CategoryName: categoryName,
		}

		maxRetries := 1
		retryDelay := 0.5
		getExpenses(client, remoteUrl, maxRetries, retryDelay, queryParam)
	},
}

func init() {
	expensesCmd.Flags().StringVarP(&startDate, "start", "s", "", "Start date from where to list expenses")
	expensesCmd.Flags().StringVarP(&endDate, "end", "e", "", "End date where to stop list expenses")

	expensesCmd.Flags().Int64VarP(&categoryId, "category-id", "c", 0, "Id of the category to filter expenses for")
	expensesCmd.Flags().StringVarP(&categoryName, "categoryname", "n", "", "Name of the category to filter expenses for")

	expensesCmd.Flags().BoolVarP(&printExpenses, "print", "p", false, "Whether to print the expense to screen or not")

	expensesCmd.MarkFlagRequired("start")
	expensesCmd.MarkFlagRequired("end")
	expensesCmd.MarkFlagsMutuallyExclusive("category-id", "categoryname")

	listCmd.AddCommand(expensesCmd)
}

func getExpenses(client *http.Client, url string, retries int, retryDelay float64, queryParam ExpensesQuery) (*http.Response, error) {
	return executeAPI(client, url, retries, retryDelay, getExpensesOnce, queryParam)
}

func getExpensesOnce(client *http.Client, remoteUrl string, queryParam interface{}) (*http.Response, error) {
	expensesQueryParam, ok := queryParam.(ExpensesQuery)
	if !ok {
		return nil, errors.New("queryParam is not of type ExpensesQuery")
	}

	finalURL, err := constructExpenseURL(remoteUrl, expensesQueryParam)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", finalURL.String(), nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	req.Header.Set("User-Agent", "budgeteer-cli")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK && resp.Request.Response == nil {
		var expenseResponse ExpenseResponse
		err = json.NewDecoder(resp.Body).Decode(&expenseResponse)
		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		if printExpenses {
			displayExpensesInTable(expenseResponse.Data)
		} else {
			p := message.NewPrinter(language.English)
			p.Printf("Expense Count: %+v\n", expenseResponse.Count)

			sum := 0

			for _, expense := range expenseResponse.Data {
				sum += expense.Amount
			}
			p.Printf("Expense Sum: %d¥\n", sum)
		}

		return nil, nil

	} else if resp.Request.Response == nil {
		fmt.Println("Expenes response was not 200")
		return resp, nil
	} else {
		fmt.Println("It seems we've got redirected")
		return resp.Request.Response, nil
	}
}

func constructExpenseURL(remoteUrl string, queryParam ExpensesQuery) (*url.URL, error) {
	// Create a URL structure for the base URL
	baseURL, err := url.Parse(remoteUrl)
	if err != nil {
		return nil, err
	}

	// Create a URL structure for the path
	expensePath := "/api/v1/expenses/create-list?format=json&huge_page=yes"
	pathURL, err := url.Parse(expensePath)
	if err != nil {
		return nil, err
	}

	// Add query parameters from queryParam
	query := pathURL.Query()
	if queryParam.CategoryID != 0 {
		query.Set("category_id", fmt.Sprintf("%d", queryParam.CategoryID))
	}
	if queryParam.CategoryName != "" {
		query.Set("category_name", queryParam.CategoryName)
	}
	if queryParam.startDate != "" {
		query.Set("start", queryParam.startDate)
	}
	if queryParam.endDate != "" {
		query.Set("end", queryParam.endDate)
	}

	// Update the path URL with the modified query parameters
	pathURL.RawQuery = query.Encode()

	// Combine the base URL and modified path URL to get the final URL
	finalURL := baseURL.ResolveReference(pathURL)

	return finalURL, nil
}

func displayExpensesToScreen(expenses []Expense) {
	fmt.Println("Expense Data:")
	for _, expense := range expenses {
		fmt.Print(expense)
	}
}

func displayExpensesInTable(expenses []Expense) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"ID", "Date", "Category Text", "Note", "Amount"})
	table.SetColWidth(40)

	p := message.NewPrinter(language.English)
	sum := 0
	for _, expense := range expenses {
		formattedAmount := p.Sprintf("%d", expense.Amount)
		row := []string{
			fmt.Sprintf("%d", expense.ID),
			expense.Date,
			expense.CategoryText,
			expense.Note,
			formattedAmount,
		}
		table.Append(row)
		sum += expense.Amount
	}

	sumFormatted := p.Sprintf("%d", sum)
	table.SetFooter([]string{"", "", "", "Total", sumFormatted})
	table.SetAutoMergeCells(false)
	table.Render()
}

// move me to a different file
type apiFunc func(client *http.Client, url string, queryParam interface{}) (*http.Response, error)

func executeAPI(client *http.Client, url string, retries int, retryDelay float64, api apiFunc, queryParam interface{}) (*http.Response, error) {
	res, err := api(client, url, queryParam)
	// The recursive function returns when we're out of retries
	if err == nil && res != nil && (res.StatusCode == http.StatusFound || res.StatusCode == http.StatusForbidden) && retries > 0 {
		// Token expired, re-login, and write the token to file
		fmt.Println("Tries to refresh login token...")
		// Try to login, and write token to file
		client, err = Login(remoteUrl, u, pw)
		if err != nil {
			return nil, err
		}

		// Wait for a while before retrying
		retryDuration := time.Duration(retryDelay * float64(time.Second))
		time.Sleep(retryDuration)

		// Retry the API call after login
		return executeAPI(client, url, retries-1, retryDelay, api, queryParam)
	}
	return res, err
}

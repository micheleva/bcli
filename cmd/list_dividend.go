package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

type DividendResponse struct {
	Count int        `json:"count"`
	Data  []Dividend `json:"results"`
}

type Dividend struct {
	ID            int64   `json:"id"`
	AccountID     int64   `json:"account"`
	AccountText   string  `json:"account_text"`
	AssetText     string  `json:"asset_text"`
	LocalAmount   float64 `json:"local_amount"`
	ForeignAmount float64 `json:"foreign_amount"`
	Currency      string  `json:"currency"`
	Rate          float64 `json:"rate"`
	RecordDate    string  `json:"record_date"`
}

func (d Dividend) String() string {
	pr := message.NewPrinter(language.English)
	ID := strconv.FormatInt(d.ID, 10)
	return pr.Sprintf(
		"ID: %s Account ID: %d Asset Text: %s Local Amount: ¥%.2f Foreign Amount: $%.2f Currency: %s Rate: %.2f Record Date: %s\n",
		ID, d.AccountID, d.AssetText, d.LocalAmount, d.ForeignAmount, d.Currency, d.Rate, d.RecordDate,
	)
}

type DividendYearlyAggregateResponse struct {
	Count int                       `json:"count"`
	Data  []DividendYearlyAggregate `json:"results"`
}

type DividendYearlyAggregate struct {
	Year   int64   `json:"year"`
	Total  float64 `json:"local_total"`
	PerDay float64 `json:"local_per_day"`
}

func (dya DividendYearlyAggregate) String() string {
	p := message.NewPrinter(language.English)
	// We need to convert the year to a string in order to avoid it to be localized (e.g. 2023 => 2,023)
	yearStr := strconv.FormatInt(dya.Year, 10)
	if yearStr == "-1" {
		yearStr = "====\nTotal"
	}
	return p.Sprintf(
		"%s : ¥%.2f (¥%.2f/day)\n",
		yearStr, dya.Total, dya.PerDay,
	)
}

type DividendsQuery struct {
	AccountID   int64
	AccountName string
	startDate   string
	endDate     string
}

func init() {
	dividendCmd.Flags().BoolVarP(&printDividends, "print", "p", false, "Prints to screen all the raw dividends details")

	dividendCmd.Flags().StringVarP(&startDate, "start", "s", "", "Start date from where to list dividends")
	dividendCmd.Flags().StringVarP(&endDate, "end", "e", "", "End date where to stop list dividends")

	listCmd.AddCommand(dividendCmd)
}

var dividendCmd = &cobra.Command{
	Use:   "dividends",
	Short: "List dividends",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		client, err := GetClientWithCookiesOrLogin()
		if err != nil {
			log.Fatal(err)
		}

		queryParam := DividendsQuery{}

		maxRetries := 1
		retryDelay := 0.5
		if printDividends {
			getDividends(client, remoteUrl, maxRetries, retryDelay, queryParam)
		}

		getDividendsYearlyAggregate(client, remoteUrl, maxRetries, retryDelay)
	},
}

func getDividends(client *http.Client, url string, retries int, retryDelay float64, queryParam DividendsQuery) (*http.Response, error) {
	return executeAPI(client, url, retries, retryDelay, getDividendsOnce, queryParam)
}

func getDividendsOnce(client *http.Client, remoteUrl string, queryParam interface{}) (*http.Response, error) {
	dividendsQueryParam, ok := queryParam.(DividendsQuery)
	if !ok {
		return nil, errors.New("queryParam is not of type DividendsQuery")
	}

	finalURL, err := constructDividendURL(remoteUrl, dividendsQueryParam)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", finalURL.String(), nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	req.Header.Set("User-Agent", "budgeteer-cli")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK && resp.Request.Response == nil {
		var dividendResponse DividendResponse
		err = json.NewDecoder(resp.Body).Decode(&dividendResponse)
		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		displayDividendsInTable(dividendResponse.Data)

		return nil, nil

	} else if resp.Request.Response == nil {
		fmt.Println("Dividend response was not 200")
		return resp, nil
	} else {
		fmt.Println("It seems we've got redirected")
		return resp.Request.Response, nil
	}
}

func constructDividendURL(remoteUrl string, queryParam DividendsQuery) (*url.URL, error) {
	// Create a URL structure for the base URL
	baseURL, err := url.Parse(remoteUrl)
	if err != nil {
		return nil, err
	}

	// Create a URL structure for the path
	dividendsPath := "/api/v1/dividends"
	pathURL, err := url.Parse(dividendsPath)
	if err != nil {
		return nil, err
	}

	// Combine the base URL and modified path URL to get the final URL
	finalURL := baseURL.ResolveReference(pathURL)

	return finalURL, nil
}

func displayDividendsInTable(dividends []Dividend) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Account", "Date", "Asset", "Jpy Amount", "USD/EUR Amount", "Rate"})
	table.SetColWidth(17)

	p := message.NewPrinter(language.English)

	for _, d := range dividends {

		amount := fmt.Sprintf("%.3f", d.LocalAmount)
		if d.LocalAmount == 0.0 {
			amount = "-"
		}

		forAmount := fmt.Sprintf("%.3f", d.ForeignAmount)
		if d.ForeignAmount == 0.0 {
			forAmount = "-"
		}
		rate := p.Sprintf("%.3f", d.Rate)

		row := []string{
			d.AccountText,
			d.RecordDate,
			d.AssetText,
			amount,
			forAmount,
			rate,
		}
		table.Append(row)
	}

	table.Render()
}

// getDividendsYearlyAggregate() tries to refresh the login token n(retries) times if the request fails with error code 403
func getDividendsYearlyAggregate(client *http.Client, url string, retries int, retryDelay float64) (*http.Response, error) {
	res, err := getDividendsYearlyAggregateOnce(client, url)
	// The recursive function returns when we're out of retries
	if err == nil && res != nil && res.StatusCode == http.StatusFound && retries > 0 {
		// Token expired, re-login, and write the token to file
		fmt.Println("Tries to refresh login token...")
		// Try to login, and write token to file
		client, err = Login(remoteUrl, u, pw)
		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		// Wait for a while before retrying
		retryDuration := time.Duration(retryDelay * float64(time.Second))
		time.Sleep(retryDuration)

		// Retry getting dividends after login
		return getDividendsYearlyAggregate(client, url, retries-1, retryDelay)
	}
	return res, err
}

func getDividendsYearlyAggregateOnce(client *http.Client, remoteUrl string) (*http.Response, error) {
	finalURL, err := constructDividendYearlyAggregateURL(remoteUrl)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", finalURL.String(), nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	req.Header.Set("User-Agent", "budgeteer-cli")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK && resp.Request.Response == nil {
		var dividendYearlyAggregateResponse DividendYearlyAggregateResponse
		err = json.NewDecoder(resp.Body).Decode(&dividendYearlyAggregateResponse)
		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		displayYearlyAggregateDividendsInTable(dividendYearlyAggregateResponse.Data)

		return nil, nil

	} else if resp.Request.Response == nil {
		fmt.Println("Dividend yearly aggregate response was not 200")
		return resp, nil
	} else {
		fmt.Println("It seems we've got redirected")
		return resp.Request.Response, nil
	}
}

func constructDividendYearlyAggregateURL(remoteUrl string) (*url.URL, error) {
	// Create a URL structure for the base URL
	baseURL, err := url.Parse(remoteUrl)
	if err != nil {
		return nil, err
	}

	// Create a URL structure for the path
	expensePath := "/api/v1/dividends/aggregate"
	pathURL, err := url.Parse(expensePath)
	if err != nil {
		return nil, err
	}

	// Combine the base URL and modified path URL to get the final URL
	finalURL := baseURL.ResolveReference(pathURL)

	return finalURL, nil
}

func displayYearlyAggregateDividendsInTable(aggs []DividendYearlyAggregate) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Year", "JPY Total", "JPy/Per day"})
	table.SetColWidth(30)

	p := message.NewPrinter(language.English)

	for _, agg := range aggs {
		totalFormatted := p.Sprintf("%.2f", agg.Total)
		perDayFormatted := p.Sprintf("%.2f", agg.PerDay)
		year := fmt.Sprintf("%v", agg.Year)
		if year == "-1" {
			year = "Total"
		}

		row := []string{
			year,
			totalFormatted,
			perDayFormatted,
		}
		table.Append(row)
	}

	table.Render()
}

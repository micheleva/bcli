/*
Copyright © 2023 Michele Valsecchi <https://gitlab.com/micheleva>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type DeleteExpensesQuery struct {
	ID int64
}

var deleteExpenseCmd = &cobra.Command{
	Use:   "expense <expense-id>",
	Short: "Delete expenses",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return fmt.Errorf("Error: accepts 1 arg, received %v", len(args))
		}

		arg := args[0]
		_, err := strconv.ParseInt(arg, 10, 64)
		if err != nil {
			return fmt.Errorf("The argument must be an integer.")
		}

		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		arg := args[0]
		expID, err := strconv.ParseInt(arg, 10, 64)
		if err != nil {
			return fmt.Errorf("The argument must be an integer.")
		}

		client, err := GetClientWithCookiesOrLogin()
		if err != nil {
			fmt.Println("Expense deletion failed!")
			return err
		}
		if client == nil {
			return fmt.Errorf("Error generating the client.")
		}

		queryParam := DeleteExpensesQuery{
			ID: expID,
		}

		maxRetries := 1
		retryDelay := 0.5
		res, err := deleteExpense(client, remoteUrl, maxRetries, retryDelay, queryParam)
		if err != nil {
			fmt.Println("Expense deletion failed")
			return err
		}
		if res.StatusCode == http.StatusNoContent {
			fmt.Println("Expense was deleted succesfully")
		} else {
			fmt.Println("Expense deletion failed")
		}
		return nil
	},
}

func init() {
	deleteCmd.AddCommand(deleteExpenseCmd)
}

func deleteExpense(client *http.Client, url string, retries int, retryDelay float64, queryParam DeleteExpensesQuery) (*http.Response, error) {
	return executeAPI(client, url, retries, retryDelay, deleteExpenseOnce, queryParam)
}

func deleteExpenseOnce(client *http.Client, remoteUrl string, queryParam interface{}) (*http.Response, error) {
	expensesQueryParam, ok := queryParam.(DeleteExpensesQuery)
	if !ok {
		return nil, errors.New("queryParam is not of type DeleteExpensesQuery")
	}

	finalURL, err := constructDeleteExpenseURL(remoteUrl, expensesQueryParam)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("DELETE", finalURL.String(), nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	req.Header.Set("User-Agent", "budgeteer-cli")
	req.Header.Set("Content-Type", "application/json")
	// IMPORTANT: do not use csrfCookie but read it from viper
	// as this API call might have been re-issued if the first one failed due to us not being logged in.
	// If the token was nil when the app was initialized, then csrfCookie variable would stay nil
	// even if we had written it again to the config file
	req.Header.Set("X-CSRFToken", viper.GetString("csrfCookie"))
	resp, err := client.Do(req)

	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusNoContent && resp.Request.Response == nil {
		// Deletion success
		return resp, nil
	} else if resp.StatusCode == http.StatusNotFound && resp.Request.Response == nil {
		return resp, fmt.Errorf("This expenses does not exist.")
	} else if resp.Request.Response == nil {
		fmt.Println("Expense delete response was not 204")
		fmt.Println(resp)
		// do not return errors on purpose
		return resp, nil
	} else {
		fmt.Println("It seems we've got redirected")
		// do not return errors on purpose
		return resp.Request.Response, nil
	}
}

func constructDeleteExpenseURL(remoteUrl string, queryParam DeleteExpensesQuery) (*url.URL, error) {
	// Create a URL structure for the base URL
	baseURL, err := url.Parse(remoteUrl)
	if err != nil {
		return nil, err
	}

	// Create a URL structure for the path
	expensePath := fmt.Sprintf("/api/v1/expenses/%d/", queryParam.ID)
	pathURL, err := url.Parse(expensePath)
	if err != nil {
		return nil, err
	}

	// Combine the base URL and modified path URL to get the final URL
	finalURL := baseURL.ResolveReference(pathURL)

	return finalURL, nil
}

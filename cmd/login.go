/*
Copyright © 2023 Michele Valsecchi <https://gitlab.com/micheleva>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type WrongCredentials struct {
	message string
}

func (e *WrongCredentials) Error() string {
	return e.message
}

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "Login into the budgeteer instance",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		_, err := GetClientWithCookiesOrLogin()
		if err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	// Bind flags and read from the configuration file
	loginCmd.Flags().StringVarP(&u, "username", "u", viper.GetString("username"), "Username (required if password is set)")
	loginCmd.Flags().StringVarP(&pw, "password", "p", viper.GetString("password"), "Password (required if username is set)")

	// TODO: are the following two lines needed?
	viper.BindPFlag("username", loginCmd.Flags().Lookup("username"))
	viper.BindPFlag("password", loginCmd.Flags().Lookup("password"))

	loginCmd.MarkFlagRequired("username")
	loginCmd.MarkFlagRequired("password")
	loginCmd.MarkFlagsRequiredTogether("username", "password")

	loginCmd.Flags().StringVarP(&remoteUrl, "remoteUrl", "a", viper.GetString("remoteurl"), "Url of the budgeteer instance (include protocol)")
	loginCmd.Flags().StringVarP(&csrfCookie, "csrfcookie", "c", viper.GetString("csrfCookie"), "Csrf cookie to set to headers")
	loginCmd.Flags().StringVarP(&sessionId, "sessionid", "s", viper.GetString("sessionId"), "SessionId cookie to set to headers")

	loginCmd.MarkFlagRequired("remoteurl")
	rootCmd.AddCommand(loginCmd)
}

func extractCookie(resp *http.Response, cookieName string) string {
	csrfToken := ""
	cookies := resp.Cookies()
	for _, cookie := range cookies {
		if cookie.Name == cookieName {
			csrfToken = cookie.Value
			break
		}
	}
	return csrfToken
}

// NOTE: this workaround is due to the presetRequiredFlags() hack we do in cmd/root.go
// without this workaround we would be writing ALL the flags, and their value to the config file
// e.g. `bcli expenses list -s xxxx -e yyy -p` would write both start and end to the config file
func updateConfig() error {
	keys := viper.AllKeys()
	keyValues := make(map[string]interface{})

	for _, key := range keys {
		keyValues[key] = viper.Get(key)
	}

	// Create a new Viper instance for temporary use
	tempViper := viper.New()

	// Do not write these keys to the config file
	patterns := map[string]bool{
		"archived":        true,
		"category-id":     true,
		"categoryname":    true,
		"config":          true,
		"end":             true,
		"help":            true,
		"no-descriptions": true,
		"print":           true,
		"start":           true,
	}

	for _, key := range keys {
		if !patterns[key] {
			value := viper.Get(key)
			tempViper.Set(key, value)
			fmt.Printf("Key: %s, Value: %v\n", key, value)
		}
	}

	// Save the filtered Viper configuration to the currently being used Vipe config file
	if configFile := viper.ConfigFileUsed(); configFile != "" {
		if err := tempViper.WriteConfigAs(configFile); err != nil {
			return err
		}
	}

	tempViper = nil

	return nil
}

func GetClientWithCookiesOrLogin() (*http.Client, error) {
	u := viper.GetString("username")
	pw := viper.GetString("password")
	remoteUrl := viper.GetString("remoteurl")
	csrfCookie := viper.GetString("csrfcookie")
	sessionId := viper.GetString("sessionid")

	var client *http.Client
	var err error

	if sessionId == "" || csrfCookie == "" {
		client, err = Login(remoteUrl, u, pw)
		if err != nil {
			return nil, err
		}
		if client == nil {
			return nil, fmt.Errorf("Error generating the client")
		}
	} else {
		client, err = createClientWithCookies(remoteUrl, csrfCookie, sessionId)
		if err != nil {
			return nil, err
		}
	}
	return client, err
}

func createClientWithCookies(remoteUrl, csrfCookie, sessionId string) (*http.Client, error) {
	u, _ := url.Parse(remoteUrl)
	jar, err := cookiejar.New(nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	client := &http.Client{
		Jar: jar,
	}
	cookies := []*http.Cookie{
		&http.Cookie{
			Name:  "csrftoken",
			Value: csrfCookie,
			Path:  "/",
		},
		&http.Cookie{
			Name:  "sessionid",
			Value: sessionId,
			Path:  "/",
		},
	}
	client.Jar.SetCookies(u, cookies)
	return client, nil
}

func Login(remoteUrl string, username string, password string) (*http.Client, error) {
	loginPath := "/accounts/login/"
	loginUrl := remoteUrl + loginPath
	jar, err := cookiejar.New(nil)
	if err != nil {
		return nil, err
	}
	client := &http.Client{
		Jar: jar,
	}

	req, err := http.NewRequest("GET", loginUrl, nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Set("User-Agent", "budgeteer-cli")

	resp, err := client.Do(req)
	if err != nil {
		return client, err
	}
	defer resp.Body.Close()

	// Extract the CSRF cookie from the response
	csrfCookie := extractCookie(resp, "csrftoken")
	if csrfCookie == "" {
		return nil, fmt.Errorf("CSRF cookie not found")
	}

	cookies := []*http.Cookie{
		&http.Cookie{
			Name:  "csrftoken",
			Value: csrfCookie,
			Path:  "/",
		},
	}
	u, _ := url.Parse(loginUrl)
	client.Jar.SetCookies(u, cookies)

	// Extract the csrf token from the form
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return client, err
	}

	csrfToken, exists := doc.Find("input[name='csrfmiddlewaretoken']").Attr("value")
	if !exists {
		return client, fmt.Errorf("CSRF token not found")
	}

	formData := url.Values{
		"username":            {username},
		"password":            {password},
		"csrfmiddlewaretoken": {csrfToken},
	}

	req2, err := http.NewRequest("POST", loginUrl, strings.NewReader(formData.Encode()))
	if err != nil {
		return nil, err
	}

	req2.Header.Set("User-Agent", "budgeteer-cli")
	req2.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp2, err := client.Do(req2)
	if err != nil {
		return nil, err
	}
	defer resp2.Body.Close()

	// TODO: handle redirect from http to https!

	// Search for credentials related errors text in the page
	err = checkForCredentialsError(resp2)
	if err != nil {
		return nil, err
	}

	if resp2.StatusCode == http.StatusOK && resp2.Request.Response != nil {
		// NOTE: on 302 the Go http.Client automatically follows the redirect and resp.Request.Response
		// contains the original request item. However, resp.StatusCode is 200, so we have to check both
		csrfCookie := extractCookie(resp2.Request.Response, "csrftoken")
		if csrfCookie == "" {
			return nil, fmt.Errorf("CSRF cookie not found")
		}

		sessionCookie := extractCookie(resp2.Request.Response, "sessionid")
		if sessionCookie == "" {
			return nil, fmt.Errorf("Session cookie not found")
		}

		cookies := []*http.Cookie{
			&http.Cookie{
				Name:  "csrftoken",
				Value: csrfCookie,
				Path:  "/",
			},
			&http.Cookie{
				Name:  "sessionid",
				Value: sessionCookie,
				Path:  "/",
			},
		}
		client.Jar.SetCookies(u, cookies)

		viper.Set("csrfcookie", csrfCookie)
		viper.Set("sessionid", sessionCookie)

		// Write csrfcookie and sessionid to file
		err = updateConfig()
		if err != nil {
			log.Fatal(err)
		}
	} else {
		if resp2.Request.Response != nil && resp2.Request.Response.StatusCode == http.StatusFound {
			log.Fatal("Invalid credentials")
			return client, nil
		}

		if csrfCookie == "" {
			return nil, fmt.Errorf("Budgeteer instance is not behaving correctly. Troubleshoot on the server side!")
		}
	}
	return client, nil
}

func checkForCredentialsError(resp *http.Response) error {
	// Search for credentials related error message.
	// Django returns a new form page with Status 200 atm so we have to confirm the HTML does not contains error
	// TODO: change this to a cleaner implementation as soon as budgeteer provides a proper API to login to the app
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return err
	}
	isCredentialsError := false
	doc.Find(".errorlist.nonfield").Each(func(i int, s *goquery.Selection) {
		if s.Text() == "Please enter a correct username and password. Note that both fields may be case-sensitive." {
			isCredentialsError = true
		}
	})
	if isCredentialsError {
		err = &WrongCredentials{
			message: "Wrong credentials provided",
		}
		return err
	}
	return nil
}

func printCookies(client *http.Client, remoteurl string) {
	categoriesPath := "/accounts/login/"
	categoriesUrl := remoteurl + categoriesPath

	u, _ := url.Parse(categoriesUrl)
	fmt.Println(client.Jar.Cookies(u))
}

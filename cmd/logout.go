/*
Copyright © 2023 Michele Valsecchi <https://gitlab.com/micheleva>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var logoutCmd = &cobra.Command{
	Use:   "logout",
	Args:  cobra.ExactArgs(0),
	Short: "clear session info",
	Long:  "Clear csrftoken and sessionId info from the config file. NOTE: the session and the csrf token are still valid!!!",
	Run: func(cmd *cobra.Command, args []string) {
		// TODO: actually post (get) to /accounts/logout to invalidate the csrftoken!
		viper.Set("csrfcookie", nil)
		viper.Set("sessionId", nil)
		fmt.Println("Removed csrftoken and sessionId info from the config file. NOTE: the session and the csrf token are still valid!!!")
	},
}

func init() {
	rootCmd.AddCommand(logoutCmd)
}

/*
Copyright © 2023 Michele Valsecchi <https://gitlab.com/micheleva>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

type CategoryResponse struct {
	Count int        `json:"count"`
	Data  []Category `json:"results"`
}

type Category struct {
	ID         int     `json:"id"`
	Text       *string `json:"text"`
	IsArchived bool    `json:"is_archived"`
}

func (c Category) String() string {
	return fmt.Sprintf(
		"\nID: %d Text: %s",
		c.ID, *c.Text,
	)
}

var categoriesCmd = &cobra.Command{
	Use:   "categories",
	Short: "List categories",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		client, err := GetClientWithCookiesOrLogin()
		if err != nil {
			log.Fatal(err)
		}

		maxRetries := 1
		retryDelay := 0.5
		getCategories(client, remoteUrl, maxRetries, retryDelay)
	},
}

func init() {
	categoriesCmd.Flags().BoolVarP(&printArchivedCat, "archived", "a", false, "Whether to print archived categories to screen or not")
	listCmd.AddCommand(categoriesCmd)
}

// getCategories tries to refresh the login token n(retries) times if the request fails with error code 403
func getCategories(client *http.Client, url string, retries int, retryDelay float64) (*http.Response, error) {
	return executeAPI(client, url, retries, retryDelay, getCategoriesOnce, nil)
}

func getCategoriesOnce(client *http.Client, remoteUrl string, queryParam interface{}) (*http.Response, error) {
	categoriesPath := "/api/v1/categories"
	categoriesUrl := remoteUrl + categoriesPath

	req, err := http.NewRequest("GET", categoriesUrl, nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	req.Header.Set("User-Agent", "budgeteer-cli")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK && resp.Request.Response == nil {
		var categoryResponse CategoryResponse
		err = json.NewDecoder(resp.Body).Decode(&categoryResponse)

		if err != nil {
			return nil, err
		}

		if true {
			displayCategoriesInTable(categoryResponse.Data)
		}

		return nil, nil
	} else if resp.Request.Response == nil {
		fmt.Println("categories response was not 200")
		return resp, nil
	} else {
		fmt.Println("It seems we've got redirected")
		return resp.Request.Response, nil
	}
}

func displayCategoriesInTable(categories []Category) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"ID", "Text", "Is Archived"})
	table.SetColWidth(30)
	// Numbers are automatically aligned on the right, this overwrites it (e.g. Category.Text = 333)
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	for _, category := range categories {
		if !category.IsArchived || printArchivedCat {
			row := []string{
				fmt.Sprintf("%v", category.ID),
				*category.Text,
				fmt.Sprintf("%t", category.IsArchived),
			}
			table.Append(row)
		}
	}

	table.Render()
}

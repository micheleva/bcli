/*
Copyright © 2023 Michele Valsecchi <https://gitlab.com/micheleva>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	cfgFile, u, pw, startDate, endDate, categoryName, remoteUrl, csrfCookie, sessionId string
	categoryId                                                                         int64
	printExpenses, printArchivedCat, printDividends                                    bool
)

var rootCmd = &cobra.Command{
	Use:   "bcli",
	Short: "Simplifying Your Finances with Command Line Power",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.bcli.yaml)")
}

// See https://github.com/spf13/viper/issues/397#issuecomment-544272457
func postInitCommands(commands []*cobra.Command) {
	for _, cmd := range commands {
		presetRequiredFlags(cmd)
		if cmd.HasSubCommands() {
			postInitCommands(cmd.Commands())
		}
	}
}

func presetRequiredFlags(cmd *cobra.Command) {
	viper.BindPFlags(cmd.Flags())
	cmd.Flags().VisitAll(func(f *pflag.Flag) {
		// See workaround from https://github.com/spf13/viper/issues/397#issuecomment-860228617
		// Pros: handles any type of flag
		// Drawbacks: does not handle .MarkFlagsRequiredTogether()
		//            i.e. if we read one value from Viper, and one from cli, it will still fail with missing required flag err
		//if viper.IsSet(f.Name) {
		//	cmd.Flags().SetAnnotation(f.Name, cobra.BashCompOneRequiredFlag, []string{"false"})
		//}

		// See workaround https://github.com/spf13/viper/issues/397#issuecomment-544272457
		// Pros: handles .MarkFlagsRequiredTogether() case
		// Drawbacks: a bit convoluted (also, atm this snippet works only for Strings as it does .GetString( )
		if viper.IsSet(f.Name) && viper.GetString(f.Name) != "" {
			cmd.Flags().Set(f.Name, viper.GetString(f.Name))
		}
	})
}

// initConfig reads in config file and ENV variables if set
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".bcli" (without extension)
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".bcli")
	}

	// Read in environment variables that match
	viper.AutomaticEnv()

	// If a config file is found, read it in
	if err := viper.ReadInConfig(); err == nil {
		postInitCommands(rootCmd.Commands())
	}

}

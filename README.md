# Budgeteer CLI (bcli) - Simplifying Your Finances with Command Line Power 💰

![GPL v.3 license](https://img.shields.io/badge/license-GPL%20v3.0-brightgreen.svg "GPL v.3 License")

## About the Project

This project is the cli for [Budgeteer](https://gitlab.com/micheleva/budgeteer)... working on this project is my way of procrastinating on shipping v1.0 of [it](https://gitlab.com/micheleva/budgeteer). I intentionally avoid using high-level HTTP/REST libraries like [Resty](https://github.com/go-resty/resty) for a specific reason: let's make the most of this procrastination by turning it into a valuable learning experience. And as for the intentionally rough architecture... it's my way of making sure I don't overengineer things and never deliver. I'll clean it up eventually, promise! 😄

## Roadmap

- [ ] Implement ONLY GET calls (and DELETE for expenses) for api/v1 listed below:
  - [x] List expenses
    - [x] Allow filtering by dates
    - [x] Allow filtering by category id or name
  - [x] List categories
  - [x] List profits
    - [ ] Allow filtering by dates
    - [x] Return yearly statistics
    - [x] Return overall statistics
      - [x] "Total $x,xxx.xx (in zz days) => $xx.xx/day"
      - [x] "Year YYYY => $xx.xx/day"
  - [ ] List assets (Do not group by account yet)
  - [x] Delete expenses by ID

## Non-goals

- Add items to the RoadMap before [Budgeteer](https://gitlab.com/micheleva/budgeteer) v1.0 is shipped.
- Implement all API endpoints
- Implement any endpoints not present in the roadmap
- Implement any PUT, POST, PATCH endpoints
- Achieve 98%+ test coverage until ALL the items in the roadmaps are implemented

## Author

Bcli was created by [Michele Valsecchi](https://gitlab.com/micheleva).

## License

GNU General Public License v3.0

See [LICENSE](./LICENSE) for the full text.
